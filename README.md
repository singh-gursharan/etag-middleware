# etag-middleware

etag-middleware adds `Etag` header in the response. For the subsequent request we need to send `If-None-Match` header having the value equal to `Etag` header value we received from the previous request. 

## Example

```go
package main

import (
  "github.com/labstack/echo"
  "gitlab.com/singh-gursharan/etag-middleware"
)

func main() {
  // Echo instance
  e := echo.New()

  // Middleware
  e.Use(etag.AddEtag)
 ....
 ....
 ....
 .... 
}
```

