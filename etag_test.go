package etag_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo"
	"gitlab.com/singh-gursharan/etag-middleware"
	"gotest.tools/assert"
)

func initEcho() *echo.Echo {
	e := echo.New()
	e.Use(etag.AddEtag)
	e.GET("/statusOk", statusOk)
	e.GET("/statusBad", statusBad)
	return e
}

type testBody struct {
	Field1 string `json:"field1"`
}

var statusOkMockResponseBody = testBody{Field1: "value1"}

type testErrorBody struct{
	Code string `json:"errorCode"`
}

var statusBadMockResponseBody = testErrorBody{Code: "error"}


func statusOk(c echo.Context) error {
	return c.JSON(http.StatusOK, statusOkMockResponseBody)
}

func statusBad(c echo.Context) error {
	return c.JSON(http.StatusBadRequest, statusBadMockResponseBody)
}

func TestEtagMiddleware(t *testing.T) {
	e := initEcho()
	req := httptest.NewRequest(echo.GET, "/statusOk", nil)
	rec := httptest.NewRecorder()
	// Using the ServerHTTP on echo will trigger the router and middleware
	e.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	responseStruct := new(testBody)
	_ = json.Unmarshal(rec.Body.Bytes(), responseStruct)
	assert.Equal(t, *responseStruct, statusOkMockResponseBody)
	etag := rec.Header().Get("ETag")
	assert.Check(t, len(etag) > 0)

	t.Run("cachingTest", func(t *testing.T) {
		req := httptest.NewRequest(echo.GET, "/statusOk", nil)
		rec := httptest.NewRecorder()
		req.Header.Set("If-None-Match", etag)
		e.ServeHTTP(rec, req)

		assert.Equal(t, rec.Body.String(), ``)
		assert.Equal(t, rec.Code, http.StatusNotModified)
		etag = rec.Header().Get("ETag")
		assert.Check(t, len(etag) > 0)
	})
}

func TestStatusBadResponse(t *testing.T) {
	e := initEcho()
	req := httptest.NewRequest(echo.GET, "/statusBad", nil)
	rec := httptest.NewRecorder()
	// Using the ServerHTTP on echo will trigger the router and middleware
	e.ServeHTTP(rec, req)
 
	assert.Equal(t, http.StatusBadRequest, rec.Code)

	responseStruct := new(testErrorBody)
	_ = json.Unmarshal(rec.Body.Bytes(), responseStruct)
	assert.Equal(t, *responseStruct, statusBadMockResponseBody)
	etag := rec.Header().Get("ETag")
	assert.Check(t, len(etag) == 0)
}